Function.prototype.before = function(beforeFn) {
  var self = this;
  return function() {
    beforeFn.apply(this, arguments);
    return self.apply(this, arguments);
  }; 
};

Function.prototype.after = function(afterFn) {
  var self = this;
  return function() {
    var ret = self.apply(this, arguments);
    afterFn.apply(this, arguments);
    return ret;
  };
};

var func = function(a) {
  console.log("2");
  return a;
};

var func1 = function() {
  console.log("1");
};

var func3 = function() {
  console.log("3");
};

var r = func.before(func1).after(func3)(99);
console.info(r);
